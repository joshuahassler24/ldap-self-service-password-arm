FROM alpine:3.13

ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION

ARG ARCH=arm
ARG TIMEZONE="US/Eastern"
ENV TIMEZONE=${TIMEZONE}

# Add Labels
LABEL maintainer="joshua@hassler.dev <Joshua Hassler>"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="joshuahassler/ldap-self-service-password-arm"
LABEL org.label-schema.description="LDAP Self Service Password Portal built for ARM64"
LABEL org.label-schema.url="https://ltb-project.org/start"
LABEL org.label-schema.vcs-url="https://gitlab.com/joshuahassler24/ldap-self-service-password-arm"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.cmd="docker run -p 80:8080 joshuahassler/ldap-self-service-password-arm"

COPY php7.sh /etc/profile.d/
COPY php7setup.sh /tmp
COPY nginx.conf /etc/nginx/

# Set up php and nginx
RUN apk add --no-cache nginx php7-fpm php7-mcrypt php7-soap php7-openssl php7-gmp \
    php7-pdo_odbc php7-json php7-dom php7-pdo php7-zip php7-mysqli php7-sqlite3 \
    php7-apcu php7-pdo_pgsql php7-bcmath php7-gd php7-odbc php7-pdo_mysql \
    php7-pdo_sqlite php7-gettext php7-xmlreader php7-xmlrpc php7-bz2 php7-iconv \
    php7-pdo_dblib php7-curl php7-ctype php7-ldap php7-mbstring \
    tzdata tar \
    && adduser -D -g 'www' www && touch /var/run/nginx.pid \
    && chown -R www:www /var/lib/nginx /var/log/nginx /var/log/php7 /var/run/nginx.pid /var/lib/nginx/html \
    && sh /tmp/php7setup.sh

ADD https://github.com/smarty-php/smarty/archive/refs/tags/v3.1.39.tar.gz /tmp
WORKDIR /tmp
RUN tar -xzvf v3.1.39.tar.gz \
    && mkdir -p /usr/share/php/smarty3/ \
    && cp -r smarty-3.1.39/libs/* /usr/share/php/smarty3/
WORKDIR /

# Set up and configure s6
ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.3/s6-overlay-${ARCH}-installer /tmp/
RUN chmod +x /tmp/s6-overlay-${ARCH}-installer \
    && /tmp/s6-overlay-${ARCH}-installer / \
    && mkdir -p /etc/services.d/nginx /etc/services.d/php-fpm7

COPY nginx-run /etc/services.d/nginx/run
COPY php-fpm7-run /etc/services.d/php-fpm7/run
RUN chmod 1555 /etc/services.d/nginx/run /etc/services.d/php-fpm7/run

# Set up and configure ldap-self-service-password
ADD https://ltb-project.org/archives/ltb-project-self-service-password-1.4.3.tar.gz /tmp/
WORKDIR /tmp
RUN tar -zxvf ltb-project-self-service-password-1.4.3.tar.gz \
    && mv ltb-project-self-service-password-1.4.3 /usr/share/self-service-password \
    && chown -R www:www /usr/share/self-service-password \
    && rm -rf /tmp/*
WORKDIR /

ENTRYPOINT ["/init"]
USER www
