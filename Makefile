BUILD_VERSION ?= "0.1.0"
IMAGE_ORG ?= "joshuahassler/ldap-self-service-password-arm"
IMAGE_TAG ?= $(BUILD_VERSION)
NO_CACHE ?= false

.PHONY: image push latest help
help:
	@echo "Makefile for building ldap-self-service-arm. Available Targets:"
	@echo "    image      Build the docker image"
	@echo "    latest     Tag current version image as latest"
	@echo "    push       Push the built image to upstream repo"
	@echo "    release    make, tag, and push a new release image"

image: Dockerfile
	docker build --no-cache=$(NO_CACHE) \
		--build-arg=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ') \
		--build-arg=BUILD_VERSION=$(BUILD_VERSION) \
		--build-arg=VCS_REF=$(shell git rev-parse --short HEAD) \
		-t $(IMAGE_ORG):$(IMAGE_TAG) \
		-f $< .

latest:
	docker tag $(IMAGE_ORG):$(IMAGE_TAG) $(IMAGE_ORG):latest

push:
	docker push $(IMAGE_ORG):$(IMAGE_TAG)
